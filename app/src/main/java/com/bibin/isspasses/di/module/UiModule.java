package com.bibin.isspasses.di.module;

import com.bibin.isspasses.data.DataHelper;
import com.bibin.isspasses.di.scope.PerActivity;
import com.bibin.isspasses.ui.passlist.PassListContract;
import com.bibin.isspasses.ui.passlist.PassListPresenter;

import dagger.Module;
import dagger.Provides;


@Module
public class UiModule {

    @PerActivity
    @Provides
    PassListContract.Presenter providePassListPresenter(DataHelper dataHelper) {
        return new PassListPresenter(dataHelper);
    }

}
