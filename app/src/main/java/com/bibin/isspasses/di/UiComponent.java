package com.bibin.isspasses.di;


import com.bibin.isspasses.di.module.UiModule;
import com.bibin.isspasses.di.scope.PerActivity;
import com.bibin.isspasses.ui.MainActivity;
import com.bibin.isspasses.ui.passlist.PassListFragment;

import dagger.Subcomponent;

@PerActivity
@Subcomponent(modules = {UiModule.class})
public interface UiComponent {
    void inject(MainActivity activity);
    void inject(PassListFragment passListFragment);
}
