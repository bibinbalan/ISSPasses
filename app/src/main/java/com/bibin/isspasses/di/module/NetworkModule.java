package com.bibin.isspasses.di.module;

import android.content.Context;

import com.bibin.isspasses.BuildConfig;
import com.bibin.isspasses.data.DataHelper;
import com.bibin.isspasses.data.DataManager;
import com.bibin.isspasses.data.RetrofitApi;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    public static final String BASE_URL = "BASE_URL";
    public static final String API_KEY = "API_KEY";



    @Provides
    @Singleton
    Gson providesGson() {
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor providesHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor).build();
    }

    @Provides
    @Singleton
    GsonConverterFactory providesGsonConverterFactory(Gson gson) {
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    RxJavaCallAdapterFactory providesRxJavaCallAdapterFactory() {
        return RxJavaCallAdapterFactory.create();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(GsonConverterFactory gsonConverterFactory, OkHttpClient okHttpClient,
                              RxJavaCallAdapterFactory rxJavaCallAdapterFactory,
                              @Named(BASE_URL) String baseUrl) {
        return new Retrofit.Builder().addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build();
    }


    @Singleton
    @Provides
    DataHelper provideIssPassRepository(RetrofitApi retrofitApi) {
        return new DataManager(retrofitApi);
    }


    @Singleton
    @Provides
    RetrofitApi providesRetrofitService(Retrofit retrofit) {
        return retrofit.create(RetrofitApi.class);
    }

    @Provides
    @Singleton
    @Named(BASE_URL)
    String providesServerUrl(Context context) {
        return BuildConfig.BASE_URL;
    }

}
