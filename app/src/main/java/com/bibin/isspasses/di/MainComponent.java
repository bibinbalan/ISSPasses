package com.bibin.isspasses.di;


import com.bibin.isspasses.di.module.ApplicationModule;
import com.bibin.isspasses.di.module.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {
        ApplicationModule.class, NetworkModule.class})
public interface MainComponent {
    UiComponent uiComponent();
}
