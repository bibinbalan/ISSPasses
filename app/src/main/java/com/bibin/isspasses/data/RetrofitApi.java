package com.bibin.isspasses.data;

import com.bibin.isspasses.data.model.IssPassesResponse;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * API interface
 */

public interface RetrofitApi {
    @GET("iss-pass.json")
    Observable<IssPassesResponse> getIssPassList(@Query("lat") double lat, @Query("lon") double lon,@Query("n") int nmbrOfPasses);
}