package com.bibin.isspasses.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by bbalan on 1/10/2018.
 */

public class IssPassesResponse {

    @SerializedName("response")
    private List<IssPass> passList;

    @SerializedName("message ")
    private String  messsage ;

    @SerializedName("request")
    private Request request;

    @SerializedName("reason")
    private String reason;


    public List<IssPass> getPassList() {
        return passList;
    }

    public void setPassList(List<IssPass> passList) {
        this.passList = passList;
    }

    public String getMesssage() {
        return messsage;
    }

    public void setMesssage(String messsage) {
        this.messsage = messsage;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
