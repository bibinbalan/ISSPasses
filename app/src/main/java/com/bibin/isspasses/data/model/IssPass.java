package com.bibin.isspasses.data.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by bbalan on 1/10/2018.
 */

public class IssPass {
    @SerializedName("duration")
    private int  duration ;

    @SerializedName("risetime")
    private long risetime;

    public long getDuration() {
        return duration;
    }

    public void setDuration(int duration) {

        this.duration = duration/1000;;
    }

    public long getRisetime() {
        return risetime;
    }

    public void setRisetime(long risetime) {
        this.risetime = risetime;
    }
}
