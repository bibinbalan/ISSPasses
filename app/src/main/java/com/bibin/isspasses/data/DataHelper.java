package com.bibin.isspasses.data;

import com.bibin.isspasses.data.model.IssPassesResponse;

import rx.Observable;

/**
 * Interface to repository
 */
public interface DataHelper {

    Observable<IssPassesResponse> getIssPassList(double lan, double lon,int nmbrOfPasses);

}
