
package com.bibin.isspasses.data;

import com.bibin.isspasses.data.model.IssPassesResponse;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * repository implementation which does the actual data fetching
 */

@Singleton
public class DataManager implements DataHelper {

    private final RetrofitApi retrofitApi;

    @Inject
    public DataManager(RetrofitApi retrofitApi) {
        this.retrofitApi = retrofitApi;

    }

    @Override
    public Observable<IssPassesResponse> getIssPassList(double lan, double lon,int nmbrOfPasses) {

        return retrofitApi.getIssPassList(lan,lon,nmbrOfPasses)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}

