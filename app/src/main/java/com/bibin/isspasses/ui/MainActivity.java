package com.bibin.isspasses.ui;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.bibin.isspasses.R;
import com.bibin.isspasses.ui.base.BaseActivity;
import com.bibin.isspasses.ui.passlist.PassListFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {


    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 3000;

    @BindView(R.id.toolbar_view)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_fragment_container);
        setUnBinder(ButterKnife.bind(this));
        getUiComponent().inject(this);
        setUp();
        checkPlayServices();
    }

    protected void setUp() {
        setupToolbar();
        addFragment(PassListFragment.newInstance());
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        setTitle(getString(R.string.iss_pass));
    }




    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

}
