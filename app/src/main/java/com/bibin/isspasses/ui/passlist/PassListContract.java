package com.bibin.isspasses.ui.passlist;


import com.bibin.isspasses.data.model.IssPass;
import com.bibin.isspasses.ui.base.BaseView;

import java.util.List;
/**
 *  call backs between PassListpreseter and PassListfrgment
 */


public interface PassListContract {

    interface View extends BaseView {
        void refreshPassList(List<IssPass> passList);
        void showEmptyView();
    }

    interface Presenter {

        void loadPassList(double lat, double lon, int n);

        public void onAttach(PassListContract.View view);

        public void onDetach();

    }
}
