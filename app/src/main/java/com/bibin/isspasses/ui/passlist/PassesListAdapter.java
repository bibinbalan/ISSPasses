package com.bibin.isspasses.ui.passlist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bibin.isspasses.R;
import com.bibin.isspasses.data.model.IssPass;
import com.bibin.isspasses.util.CommonUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PassesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<IssPass> mPassList;

    public PassesListAdapter(List<IssPass> passList) {
        mPassList = passList;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((ItemViewHolder) holder).onBind(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //TODO
            return new ItemViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.item_iss_pass_view, parent, false));
    }

    @Override
    public int getItemCount() {
        if (mPassList != null && mPassList.size() > 0) {
            return mPassList.size();
        } else {
            return 0;
        }
    }

    public void addItems(List<IssPass> passList) {
        mPassList.addAll(passList);
        notifyDataSetChanged();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pass_date)
        TextView tvPassDate;

        @BindView(R.id.pass_time)
        TextView tvPassTime;

        @BindView(R.id.duration)
        TextView tvDuration;


        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void onBind(int position) {

            IssPass pass = mPassList.get(position);
            tvPassDate.setText(CommonUtils.getDateFormatted(pass.getRisetime()));
            tvPassTime.setText(CommonUtils.getTimeFormatted(pass.getRisetime()));
            tvDuration.setText(CommonUtils.getTimeDuration(pass.getDuration()));
        }
    }

}
