package com.bibin.isspasses.ui.passlist;


import com.bibin.isspasses.R;
import com.bibin.isspasses.data.DataHelper;
import com.bibin.isspasses.data.model.IssPassesResponse;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.Subscription;

/**
 * The presenter that handling backend operation and controls the logic
 */
public class PassListPresenter implements PassListContract.Presenter {

    public static final String SERVICE_SUCCESS = "success";

    private PassListContract.View mView;
    private DataHelper mDataHelper;
    private Subscription subCription;

    @Inject
    public PassListPresenter(DataHelper dataHelper) {
        mDataHelper = dataHelper;
    }


    @Override
    public void loadPassList(double lat, double lon, int n) {

        if (mView.isNetworkConnected()) {
            mView.showLoading();
            Observable<IssPassesResponse> observer = mDataHelper.getIssPassList(lat, lon, n);
            onServiceResult(observer);
        } else {
            mView.onError(R.string.network_error);
        }

    }

    private void onServiceResult(Observable<IssPassesResponse> observer) {
       subCription= observer.subscribe(new Observer<IssPassesResponse>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                mView.hideLoading();
                mView.onError(R.string.error);
            }

            @Override
            public void onNext(IssPassesResponse response) {
                mView.hideLoading();
                if (response != null || SERVICE_SUCCESS.equals(response.getMesssage())) {
                    refreshUi(response);
                } else {
                    if (response != null && response.getReason() != null) {
                        mView.showMessage(response.getReason());
                    } else {
                        mView.onError(R.string.error);
                    }

                }
            }
        });
    }

    public void refreshUi(IssPassesResponse response) {
        if (response.getPassList() == null || response.getPassList().size() < 1) {
            mView.showEmptyView();
        } else {
            mView.refreshPassList(response.getPassList());
        }
    }

    @Override
    public void onAttach(PassListContract.View view) {
        mView = view;
    }

    @Override
    public void onDetach() {
        mView.hideLoading();
        if(subCription!=null)
            subCription.unsubscribe();
        mView = null;
    }
}
