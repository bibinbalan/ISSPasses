package com.bibin.isspasses.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

import com.bibin.isspasses.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;


/**
 * Common utility fuctions used in the application
 */

public class CommonUtils {

    private static SimpleDateFormat TIME_FORMAT_AM_PM = new SimpleDateFormat("hh:mm:ss a");
    private static SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("hh:mm:ss");
    private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");


    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static void showLocationOffAlert(final Context context, DialogInterface.OnClickListener cancelListner) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", cancelListner);
        dialog.show();
    }

    public static void showErrorDialog(Context context, String  msg) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("Error")
                .setMessage(msg)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        paramDialogInterface.cancel();
                    }
                });

        dialog.show();
    }


    public static boolean isLocationEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    public static String format(long timeInMillis, SimpleDateFormat format) {

        String dateString = "";
        try {
            //Date date = new Date(Long.parseLong("1515640730"));
            Date date = new Date(timeInMillis * 1000L);
            dateString = format.format(date);
        } catch (NumberFormatException e) {
            dateString = "";
        }
        return dateString;

    }

    public static String getDateFormatted(long unixTime) {
        return format(unixTime, DATE_FORMAT);
    }

    public static String getTimeFormatted(long unixTime) {
        return format(unixTime, TIME_FORMAT_AM_PM);
    }

    public static String getTimeDuration(long uptime) {

        long hours = TimeUnit.SECONDS
                .toHours(uptime);
        uptime -= TimeUnit.HOURS.toSeconds(hours);

        long minutes = TimeUnit.SECONDS
                .toMinutes(uptime);
        uptime -= TimeUnit.MINUTES.toSeconds(minutes);
        String duration = "";
        if (hours > 0) {
            duration += hours + " hrs ";
        }
        if (minutes > 0) {
            duration += minutes + " min ";
        }
        if (minutes > 0) {
            duration += uptime + " sec";
        }
        return duration;
    }


}
